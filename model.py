from pony.orm import *


db = Database()


class Urls(db.Entity):
    id = PrimaryKey(int, auto=True)
    source = Required(LongStr)
    target = Required(LongStr)

try:
    from db_local import *
    db = getDb(db)
except Exception as e:
    print str(e)
    print "Cannot connect to database"

db.generate_mapping(create_tables=True)