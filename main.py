from model import *
import csv

import sys
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

filename = "src/lista25.log"
with open(filename) as f:
    content = f.readlines()
    for linea in content:
        string = linea.split('INFO:root:;')[1].split(';From;')
        source = string[0]
        target = string[1].split("\n")[0]
        if source and target:
            with db_session:
                try:
                    if not Urls.get(source=source,target=target):
                        Urls(source=source, target=target)
                except Exception as e:
                    print str(e)
                    pass

filename = "src/lista25.csv"
with open(filename,'wb') as f:
    wr = csv.writer(f, quoting=csv.QUOTE_ALL)
    wr.writerow(['source','target'])
    listaUrls = list()
    with db_session:
        for unaUrl in select(p for p in Urls):
            wr.writerow([unaUrl.source,unaUrl.target])

